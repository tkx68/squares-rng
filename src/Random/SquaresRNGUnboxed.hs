{-# LANGUAGE Strict #-}

-- |
-- Module      : Random.SquaresRNG
-- Description : Counter based pseudo RNG
-- Copyright   : (c) Torsten Kemps-Benedix, 2020
-- License     : BSD-3
-- Maintainer  : tkx68@icloud.com
-- Stability   : experimental
-- Portability : POSIX
--
-- This package provides an implementation of the counter based pseudo
-- random number generator (RNG) described in: Bernard Widynski:
-- "Squares: A Fast Counter-Based RNG", 2020.
--
-- This module uses unboxed types. Please activate the GHC extension
-- MagicHash and import GHC.Prim.
module Random.SquaresRNGUnboxed
  ( squares#,
    keys#,
  )
where

import Data.List ((!!))
import Data.Ord (Ord (max, min))
import GHC.Prim
import GHC.Types (Int, Word (W#))
import Random.Squares.Keys (ks)

-- | Generate a pseudo random number given a counter and a key using 3 iterations.
-- Same es 'Random.SquaresRNG.squares' but unboxed.
squares# :: Word# -> Word# -> Word#
{-# INLINE squares# #-}
squares# ctr key =
  let x = ctr `timesWord#` key
      z = x `plusWord#` key
      x1 = squareRound# x x -- round 1
      x2 = squareRound# x1 z -- round 2
   in uncheckedShiftRL# ((x2 `timesWord#` x2) `plusWord#` x) 32# -- round 3

squareRound# :: Word# -> Word# -> Word#
{-# INLINE squareRound# #-}
squareRound# x y =
  let x' = (x `timesWord#` x) `plusWord#` y
   in (uncheckedShiftRL# x' 32#) `or#` (uncheckedShiftL# x' 32#)

-- | Get a key from a list of 25000 pre-calculated keys indexed from 0 to 24999.
--  Same es 'Random.SquaresRNG.squares' but unboxed.
keys# :: Int -> Word#
keys# i =
  let i' = max 0 (min 24999 i)
      !(W# k) = ks !! i'
   in k
