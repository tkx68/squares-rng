{-# LANGUAGE Strict #-}

-- |
-- Module      : Random.SquaresRNG
-- Description : Counter based pseudo RNG
-- Copyright   : (c) Torsten Kemps-Benedix, 2020
-- License     : BSD-3
-- Maintainer  : tkx68@icloud.com
-- Stability   : experimental
-- Portability : POSIX
--
-- This package provides an implementation of the counter based pseudo
-- random number generator (RNG) described in: Bernard Widynski:
-- "Squares: A Fast Counter-Based RNG", 2020.
--
-- Example usage:
--
-- @
-- putStrLn ("Demo for Squares RNG" :: Text)
-- let k = keys 0
-- putStrLn $ ("Use key: " :: Text) <> (show k)
-- putStrLn ("First 100 pseudo random numbers:" :: Text)
-- forM_ [0 .. 99] $ \i -> do
--   let r = squares i k
--   putStrLn ((show i :: Text) <> "\t" <> show r)
-- @
module Random.SquaresRNG
  ( squares,
    keys,
  )
where

import GHC.Types
import Random.SquaresRNGUnboxed

-- | Generate a pseudo random number given a counter and a key using 3 iterations.
squares :: Word -> Word -> Word
{-# INLINE squares #-}
squares (W# ctr) (W# key) = W# (squares# ctr key)

-- | Get a key from a list of 25000 pre-calculated keys index from 0 to 24999.
keys :: Int -> Word
keys i = W# (keys# i)
