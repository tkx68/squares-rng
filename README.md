# squares-rng

This package provides an implementation of the counter based pseudo random number generator (RNG) described in:

Bernard Widynski: "Squares: A Fast Counter-Based RNG", 2020.

Have a look at the demo application for a usage example.

## Build

Build with stack:

> stack build

Build with LLVM support:

> stack build --flag squares-rng:llvm

## Run demo

> stack exec demo-squares

This implementation runs a modest 20%-30% slower compared with the optimized original C demo program.

```C
#include <stdio.h>
#include "squares.h"

#define two32 4294967296.
#define billion 1000000000
#define k 0x548c9decbce65297

int main () {
   uint64_t i, n = billion;
   uint64_t sum = 0;
   for (i=0;i<n;i++) {
       sum += squares(i,k);
   }
   printf("\naverage = %lf\n",(double)sum/two32/n);
   return 0;
}
```
