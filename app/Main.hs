{-# LANGUAGE NumericUnderscores #-}

module Main where

import Data.Semigroup
import Protolude
import Random.SquaresRNG

main :: IO ()
main = do
  putStrLn ("Demo for Squares RNG" :: Text)
  let k = keys 0
  putStrLn $ ("Use key: " :: Text) <> (show k)
  putStrLn ("First 100 pseudo random numbers:" :: Text)
  forM_ [0 .. 99] $ \i -> do
    let r = squares i k
    putStrLn ((show i :: Text) <> "\t" <> show r)
  putStrLn ("Mean of 1 billion random numbers from the intervall [0,1]:" :: Text)
  let two32 = 4294967296.0 :: Double
      billion = 1_000_000_000 :: Word
      s = sum (fmap (flip squares k) [0 .. billion -1])
      mean = fromIntegral s / two32 / fromIntegral (toInteger billion) :: Double
  putStrLn (show mean :: Text)
